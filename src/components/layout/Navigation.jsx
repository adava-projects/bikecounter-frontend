import { NavLink } from "react-router-dom";
import clsx from "clsx";
import { useIsAuthenticated } from "react-auth-kit";

const Navigation = () => {
  const isAuthenticated = useIsAuthenticated();

  return (
    <nav className="navbar navbar-expand-sm navbar-light bg-light">
      <div className="container">
        <ul className="navbar-nav w-100">
          <li className="nav-item">
            <NavLink
              to="/"
              className={({ isActive }) =>
                clsx("nav-link", isActive && "active")
              }
            >
              Accueil
            </NavLink>
          </li>
          {isAuthenticated() ? (
            <>
              <li className="nav-item">
                <NavLink
                  to="/formulaire"
                  className={({ isActive }) =>
                    clsx("nav-link", isActive && "active")
                  }
                >
                  Formulaire
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink
                  to="/graphs"
                  className={({ isActive }) =>
                    clsx("nav-link", isActive && "active")
                  }
                >
                  Graphs
                </NavLink>
              </li>
            </>
          ) : null}
          <li className="nav-item ms-auto">
            <NavLink
              to="/connexion"
              className={({ isActive }) =>
                clsx("nav-link", isActive && "active")
              }
            >
              Login
            </NavLink>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navigation;
