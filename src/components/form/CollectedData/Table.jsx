import {
  modeDeTransportOptions,
  typeDeVoieOptions,
} from "../../../data/constants";

function Table({ direction, data, name, setTableValue }) {
  return (
    <>
      <h5 className="mt-5">{direction}</h5>
      <table className="table">
        <thead>
          <tr>
            <th scope="col" key=""></th>
            {typeDeVoieOptions.map((typeDeVoie) => (
              <th scope="col" key={typeDeVoie.value}>
                {typeDeVoie.label}
              </th>
            ))}
          </tr>
        </thead>
        <tbody>
          {modeDeTransportOptions.map((modeDeTransport) => (
            <tr key={modeDeTransport.value}>
              <th scope="row">{modeDeTransport.label}</th>
              {typeDeVoieOptions.map((typeDeVoie) => (
                <td key={`${modeDeTransport.value}-${typeDeVoie.value}`}>
                  <input
                    type="number"
                    value={data[typeDeVoie.value][modeDeTransport.value]}
                    onChange={(event) => {
                      setTableValue(
                        name,
                        typeDeVoie.value,
                        modeDeTransport.value,
                        event.target.value
                      );
                    }}
                    className="form-control"
                    required={
                      typeDeVoie.value === "chaussee" &&
                      modeDeTransport.value === "velo"
                    }
                  />
                </td>
              ))}
            </tr>
          ))}
        </tbody>
      </table>
    </>
  );
}

export default Table;
