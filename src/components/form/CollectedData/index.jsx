import { useEffect } from "react";
import Table from "./Table";

function CollectedData({
  locations,
  location,
  setLocation,
  listDirectionTables,
  directionTables,
  setTableValue,
}) {
  const selectLocation = (event) => {
    setLocation(event.target.value);
  };

  useEffect(() => {
    listDirectionTables(locations[location]);
  }, [locations, location]);

  return (
    <>
      <h4 className="mt-5 mb-0">Données collectées</h4>
      <div className="col-12">
        <label htmlFor="collectPlace" className="form-label">
          Lieu du comptage
        </label>
        <select
          className="form-select"
          id="collectPlace"
          aria-label="Default select example"
          value={location}
          onChange={selectLocation}
          required
          aria-describedby="selectHelpBlock"
        >
          <option value="" hidden>
            Sélectionnez le lieu du comptage
          </option>
          {Object.keys(locations).map((label) => (
            <option value={label} key={label}>
              {label}
            </option>
          ))}
        </select>
        <div id="selectHelpBlock" className="form-text">
          Sélectionnez le lieu de votre comptage pour pouvoir ajouter vos
          données
        </div>
      </div>

      <div id="counters">
        {Object.entries(directionTables)?.map(([key, tableData]) => (
          <Table
            key={key}
            setTableValue={setTableValue}
            {...tableData}
            name={key}
          />
        )) || null}
      </div>
    </>
  );
}

export default CollectedData;
