import { registerLocale } from "react-datepicker";
import fr from "date-fns/locale/fr";
registerLocale("fr", fr);
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

function DateTime({
  date,
  setDate,
  beginTime,
  setBeginTime,
  endTime,
  setEndTime,
}) {
  return (
    <>
      <h4 className="mt-5 mb-0">Date et heure du comptage</h4>
      <div className="col-md-4">
        <label htmlFor="collectDate" className="form-label">
          Date du comptage
        </label>
        <DatePicker
          className="form-control"
          dateFormat="dd/MM/yyyy"
          id="collectDate"
          locale="fr"
          onChange={setDate}
          required
          selected={date}
        />
      </div>

      <div className="col-md-4">
        <label htmlFor="collectTimeStart" className="form-label">
          Heure de début du comptage
        </label>
        <DatePicker
          className="form-control"
          dateFormat="HH:mm"
          id="collectTimeStart"
          locale="fr"
          onChange={setBeginTime}
          selected={beginTime}
          showTimeSelect
          showTimeSelectOnly
          timeCaption="Heure"
          timeIntervals={15}
        />
      </div>

      <div className="col-md-4">
        <label htmlFor="collectTimeEnd" className="form-label">
          Heure de fin du comptage
        </label>
        <DatePicker
          className="form-control"
          dateFormat="HH:mm"
          id="collectTimeEnd"
          locale="fr"
          onChange={setEndTime}
          selected={endTime}
          showTimeSelect
          showTimeSelectOnly
          timeCaption="Heure"
          timeIntervals={15}
        />
      </div>
    </>
  );
}

export default DateTime;
