function Weather({
  rainOptions,
  rain,
  setRain,
  windOptions,
  wind,
  setWind,
  visibilityOptions,
  visibility,
  setVisibility,
}) {
  const selectRain = (event) => {
    setRain(event.target.value);
  };

  const selectWind = (event) => {
    setWind(event.target.value);
  };

  const selectVisibility = (event) => {
    setVisibility(event.target.value);
  };

  return (
    <>
      <h4 className="mt-5 mb-0">Météo</h4>
      <div className="col-md-4">
        <label htmlFor="rain" className="form-label">
          Pluie
        </label>
        <select
          className="form-select"
          id="rain"
          aria-label="Default select example"
          value={rain}
          onChange={selectRain}
          required
        >
          <option value="" hidden>
            Selectionnez une réponse
          </option>
          {rainOptions.map(({ value, label }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
      </div>

      <div className="col-md-4">
        <label htmlFor="wind" className="form-label">
          Vent
        </label>
        <select
          className="form-select"
          id="wind"
          aria-label="Default select example"
          value={wind}
          onChange={selectWind}
          required
        >
          <option value="" hidden>
            Selectionnez une réponse
          </option>
          {windOptions.map(({ label, value }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
      </div>

      <div className="col-md-4">
        <label htmlFor="visibility" className="form-label">
          Visibilité
        </label>
        <select
          className="form-select"
          id="visibility"
          aria-label="Default select example"
          value={visibility}
          onChange={selectVisibility}
          required
        >
          <option value="" hidden>
            Selectionnez une réponse
          </option>
          {visibilityOptions.map(({ label, value }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

export default Weather;
