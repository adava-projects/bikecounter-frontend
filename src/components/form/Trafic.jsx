function Trafic({ trafficOptions, traffic, setTraffic }) {
  const selectTraffic = (event) => {
    setTraffic(event.target.value);
  };

  return (
    <>
      <h4 className="mt-5 mb-0">Trafic</h4>
      <div className="col-md-4">
        <label htmlFor="trafic" className="form-label">
          État du trafic
        </label>
        <select
          className="form-select"
          id="traffic"
          aria-label="Default select example"
          value={traffic}
          onChange={selectTraffic}
          required
        >
          <option value="" hidden>
            Selectionnez une réponse
          </option>
          {trafficOptions.map(({ value, label }) => (
            <option value={value} key={value}>
              {label}
            </option>
          ))}
        </select>
      </div>
    </>
  );
}

export default Trafic;
