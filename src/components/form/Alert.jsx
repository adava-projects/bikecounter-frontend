import {
  CheckCircleFill,
  ExclamationTriangleFill,
  InfoCircleFill,
} from "react-bootstrap-icons";

function Alert({ type = "primary", children, noIcon = false }) {
  let Icon = null;
  switch (type) {
    case "primary":
      Icon = InfoCircleFill;
      break;
    case "success":
      Icon = CheckCircleFill;
      break;
    case "warning":
    case "danger":
      Icon = ExclamationTriangleFill;
  }

  return (
    <div
      className={`alert alert-${type} d-flex align-items-center`}
      role="alert"
    >
      {noIcon ? null : <Icon className="me-2" />}
      <div>{children}</div>
    </div>
  );
}

export default Alert;
