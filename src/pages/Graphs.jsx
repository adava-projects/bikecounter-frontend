import axios from "axios";
import { useEffect, useState } from "react";
import Plot from "react-plotly.js";
import Alert from "../components/form/Alert";

function Graphs() {
  const [loading, setLoading] = useState(true);
  const [graphs, setGraphs] = useState([]);
  const [map, setMap] = useState({});
  const [selectedGraph, setSelectedGraph] = useState(0);

  useEffect(() => {
    axios
      .get(`${import.meta.env.VITE_API_ENDPOINT}/api/graphs`)
      .then(({ data: { graphs_data, map_data } }) => {
        setLoading(false);
        setGraphs(graphs_data.map((graph) => JSON.parse(graph)));
        setMap(JSON.parse(map_data));
      });
  }, []);

  const handleRelayout = (graph, index, event) => {
    let index_prem_mois, index_der_mois;
    if (event["xaxis.autorange"] === true) {
      index_prem_mois = 0;
      index_der_mois = graph.data[0].x.length - 1;
    } else if (event["xaxis.range[0]"] === undefined) {
      return;
    } else {
      const x0 = event["xaxis.range[0]"];
      const x1 = event["xaxis.range[1]"];

      let date = new Date(x0);
      date.setHours(0, 0, 0, 0);
      date.setDate(1);
      date = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
      let iso_string = date.toISOString().split(".")[0];
      index_prem_mois = graph.data[0].x.indexOf(iso_string);

      date = new Date(x1);
      date.setHours(0, 0, 0, 0);
      date.setDate(1);
      date = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
      iso_string = date.toISOString().split(".")[0];
      index_der_mois = graph.data[0].x.indexOf(iso_string);
    }
    for (let i = 0; i < graph.data.length / 2; i++) {
      const keys = graph.data[i].x;
      const values = graph.data[i].y;
      const time_serie = {};
      keys.forEach((key, j) => (time_serie[key] = values[j]));

      const payload = {
        time_serie,
        index_prem_mois,
        index_der_mois,
      };

      const title = graph.layout.title.text;

      if (title.includes("selon le type de vélo")) {
        if (i === 0) {
          payload["tendance_prefix"] = "Tendance VAE";
        } else {
          payload["tendance_prefix"] = "Tendance vélo classique";
        }
      }

      fetch(`${import.meta.env.VITE_API_ENDPOINT}/api/msg_perc_croissance`, {
        method: "POST",
        body: JSON.stringify(payload),
      }).then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        response.json().then((raw_data) => {
          const annotation = graph.layout.annotations[i];
          annotation.text = raw_data.msg;
          setGraphs((graphs) => {
            const newGraphs = structuredClone(graphs);
            newGraphs[index].layout.annotations = [
              ...(i === 0 ? [] : newGraphs[index].layout.annotations),
              annotation,
            ];
            return newGraphs;
          });
        });
      });
    }
  };

  console.log(graphs[selectedGraph - 1]);

  return (
    <>
      <Alert noIcon={true}>
        <p>
          Pour la lisibilité des graphiques, les données manquantes (mois sans
          comptages) ont été remplacées par une estimation : la moyenne annuelle
          + la composante saisonnière liée au mois.
        </p>
        <p>
          Par exemple, si on n&apos;a pas la donnée pour avril 2020 à Gambetta,
          on l&apos;a estimée par la somme :
        </p>
        <ul>
          <li>du nombre de comptages moyen en 2020 à Gambetta : 36,8</li>
          <li>
            et de vélos supplémentaires en avril en moyenne par rapport à la
            moyenne annuelle : 1,7
          </li>
        </ul>
        <p>Donc on indique 38,5 vélos comptés.</p>
      </Alert>
      {loading ? (
        <div className="text-center">
          <div className="spinner-border" role="status">
            <span className="visually-hidden">Chargement...</span>
          </div>
        </div>
      ) : (
        <>
          <select
            onChange={(event) => {
              setSelectedGraph(parseInt(event.target.value));
            }}
          >
            {/* <li key="mapTab" className="nav-item" role="presentation">
              <button
                className="nav-link"
                onClick={() => {
                  setSelectedGraph(0);
                }}
              > */}
            <option key={0} value={0}>
              Cartographie
            </option>
            {/* </button>
            </li> */}
            {graphs.map((graph, index) => {
              return (
                // <li key={index} className="nav-item" role="presentation">
                //   <button
                //     className="nav-link"
                //     onClick={() => {
                //       setSelectedGraph(index + 1);
                //     }}
                //   >
                //     {graph.layout.title.text.split("<br>")[0]}
                //   </button>
                // </li>
                <option key={index + 1} value={index + 1}>
                  {graph.layout.title.text.split("<br>")[0]}
                </option>
              );
            })}
          </select>

          {selectedGraph === 0 ? (
            <Plot {...map} />
          ) : (
            <Plot
              // key={index}
              {...graphs[selectedGraph - 1]}
              onRelayout={(event) => {
                handleRelayout(
                  graphs[selectedGraph - 1],
                  selectedGraph - 1,
                  event
                );
              }}
            />
          )}
        </>
      )}
    </>
  );
}

export default Graphs;
