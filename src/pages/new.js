plot_div = document.getElementById("plot_div");
next_btn = document.getElementById("next");
prev_btn = document.getElementById("prev");
map = document.getElementById("map");
plot_index = 0;
plot_data = null;

function generate_plot(plot_index) {
  Plotly.purge(plot_div);
  Plotly.plot(
    plot_div,
    plot_data[plot_index].data,
    plot_data[plot_index].layout
  );

  plot_div.on("plotly_relayout", function (eventdata) {
    if (eventdata["xaxis.autorange"] === true) {
      index_prem_mois = 0;
      index_der_mois = plot_data[plot_index].data[0].x.length - 1;
    } else if (eventdata["xaxis.range[0]"] === undefined) {
      return;
    } else {
      x0 = eventdata["xaxis.range[0]"];
      x1 = eventdata["xaxis.range[1]"];

      date = new Date(x0);
      date.setHours(0, 0, 0, 0);
      date.setDate(1);
      date = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
      iso_string = date.toISOString().split(".")[0];
      index_prem_mois = plot_data[plot_index].data[0].x.indexOf(iso_string);

      date = new Date(x1);
      date.setHours(0, 0, 0, 0);
      date.setDate(1);
      date = new Date(date.getTime() - date.getTimezoneOffset() * 60 * 1000);
      iso_string = date.toISOString().split(".")[0];
      index_der_mois = plot_data[plot_index].data[0].x.indexOf(iso_string);
    }

    for (let i = 0; i < plot_data[plot_index].data.length / 2; i++) {
      console.log(plot_data[plot_index].data);
      keys = plot_data[plot_index].data[i].x;
      values = plot_data[plot_index].data[i].y;
      var result = {};
      keys.forEach((key, i) => (result[key] = values[i]));

      // if substring "selon le type de vélo" is in the title
      // add "tendance_prefix": "Tendance VAE" to the payload if i == 0
      // else add "tendance_prefix": "Tendance Vélo"

      payload = {
        time_serie: result,
        index_prem_mois: index_prem_mois,
        index_der_mois: index_der_mois,
      };

      title = plot_data[plot_index].layout.title.text;

      if (title.includes("selon le type de vélo")) {
        if (i === 0) {
          payload["tendance_prefix"] = "Tendance VAE";
        } else {
          payload["tendance_prefix"] = "Tendance vélo classique";
        }
      }

      fetch(
        "https://adava-bikecounter-backend.herokuapp.com/api/msg_perc_croissance",
        {
          method: "POST",
          body: JSON.stringify(payload),
        }
      ).then((response) => {
        if (!response.ok) {
          throw new Error(`HTTP error! Status: ${response.status}`);
        }
        response.json().then((raw_data) => {
          annotation = plot_div.layout.annotations[i];
          annotation.text = raw_data.msg;
          Plotly.relayout(plot_div, { "layout.annotations": [annotation] });
        });
      });
    }
  });
}

next_btn.onclick = function () {
  if (plot_index < plot_data.length - 1) {
    plot_index += 1;
    generate_plot(plot_index);
  }
};

prev_btn.onclick = function () {
  if (plot_index > 0) {
    plot_index -= 1;
    generate_plot(plot_index);
  }
};

fetch("https://adava-bikecounter-backend.herokuapp.com/api/graphs").then(
  (response) => {
    if (!response.ok) {
      throw new Error(`HTTP error! Status: ${response.status}`);
    }
    response.json().then((raw_data) => {
      data_json = raw_data.graphs_data;
      plot_data = data_json.map((json_element) => JSON.parse(json_element));
      map_data = JSON.parse(raw_data.map_data);
      Plotly.plot(map, map_data.data, map_data.layout);
      generate_plot(0);
    });
  }
);
