import { useState } from "react";
import { useSignIn, useSignOut, useIsAuthenticated } from "react-auth-kit";
import axios from "axios";

function Connexion() {
  const isAuthenticated = useIsAuthenticated();
  const signIn = useSignIn();
  const signOut = useSignOut();

  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const onSubmit = (e) => {
    e.preventDefault();
    axios
      .post(`${import.meta.env.VITE_API_ENDPOINT}/api/login`, {
        username,
        password,
      })
      .then((res) => {
        console.log("res", res);
        if (res.status === 200) {
          if (
            signIn({
              token: res.data.access_token,
              expiresIn: 24 * 60,
              tokenType: "Bearer",
              // authState: res.data.authUserState,
              // refreshToken: res.data.refreshToken, // Only if you are using refreshToken feature
              // refreshTokenExpireIn: res.data.refreshTokenExpireIn,
            })
          ) {
            console.log("signIn success");
            // Only if you are using refreshToken feature
            // Redirect or do-something
          } else {
            console.log("signIn failed");
            //Throw error
          }
        }
      });
  };

  const handleUsernameChange = (e) => {
    setUsername(e.target.value);
  };

  const handlePasswordChange = (e) => {
    setPassword(e.target.value);
  };

  return (
    <div>
      <h2>Login</h2>
      {!isAuthenticated() ? (
        <form action="#">
          <div>
            <input
              type="text"
              placeholder="Username"
              onChange={handleUsernameChange}
              value={username}
            />
          </div>
          <div>
            <input
              type="password"
              placeholder="Password"
              onChange={handlePasswordChange}
              value={password}
            />
          </div>
          <button onClick={onSubmit} type="submit">
            Login Now
          </button>
        </form>
      ) : (
        <button
          onClick={() => {
            console.log("signOut");
            signOut();
          }}
        >
          Logout
        </button>
      )}
    </div>
  );
}

export default Connexion;
