import { useState, useReducer, useEffect } from "react";

import locations from "../data/locations.json";
import {
  trafficOptions,
  rainOptions,
  windOptions,
  visibilityOptions,
  typeDeVoieOptions,
  modeDeTransportOptions,
} from "../data/constants";

import Weather from "../components/form/Weather";
import DateTime from "../components/form/DateTime";
import Trafic from "../components/form/Trafic";
import CollectedData from "../components/form/CollectedData";
import clsx from "clsx";
import Alert from "../components/form/Alert";

const initialDirectionTables = {};

function reducer(state, action) {
  switch (action.type) {
    case "list": {
      if (action.payload) {
        const newDirectionTables = {};
        action.payload.forEach(({ name, direction }) => {
          newDirectionTables[name] = {
            direction,
            data: typeDeVoieOptions.reduce((acc, { value }) => {
              return {
                ...acc,
                [value]: modeDeTransportOptions.reduce((acc, { value }) => {
                  return {
                    ...acc,
                    [value]: "",
                  };
                }, {}),
              };
            }, {}),
          };
        });
        return newDirectionTables;
      } else {
        return initialDirectionTables;
      }
    }
    case "change":
      return {
        ...state,
        [action.payload.name]: {
          ...state[action.payload.name],
          data: {
            ...state[action.payload.name].data,
            [action.payload.path]: {
              ...state[action.payload.name].data[action.payload.path],
              [action.payload.vehicle]: action.payload.value,
            },
          },
        },
      };
    default:
      throw new Error();
  }
}

const d = new Date();
d.setHours(0, 0, 0, 0);

function FormPage() {
  const [date, setDate] = useState(new Date());
  const [beginTime, setBeginTime] = useState(d);
  const [endTime, setEndTime] = useState(d);
  const [location, setLocation] = useState("");
  const [traffic, setTraffic] = useState("");
  const [rain, setRain] = useState("");
  const [wind, setWind] = useState("");
  const [visibility, setVisibility] = useState("");
  const [directionTables, dispatch] = useReducer(
    reducer,
    initialDirectionTables
  );
  const [wasValidated, setWasValidated] = useState(false);
  const [alerts, setAlerts] = useState([]);

  useEffect(() => {
    if (wasValidated) setWasValidated(false);
  }, [
    date,
    beginTime,
    endTime,
    location,
    traffic,
    rain,
    wind,
    visibility,
    directionTables,
  ]);

  const listDirectionTables = (street) => {
    dispatch({
      type: "list",
      payload: street,
    });
  };

  const setTableValue = (name, path, vehicle, value) => {
    if (value >= 0)
      dispatch({
        type: "change",
        payload: { name, path, vehicle, value },
      });
  };

  const handleSubmit = (e) => {
    if (!e.target.checkValidity()) {
      e.stopPropagation();
    }
    setWasValidated(true);
    e.preventDefault();

    const tableValues = Object.values(directionTables)
      .map(({ data }) =>
        Object.values(data).map((typeDeVoie) => Object.values(typeDeVoie))
      )
      .flat(2);
    const allCellsAreEmpty = tableValues.every((cell) => cell === "");
    if (allCellsAreEmpty) {
      setAlerts((alerts) => [
        ...alerts,
        {
          key: "celluleObligatoire",
          type: "danger",
          children: "Veuillez remplir au moins une cellule",
        },
      ]);
    }

    const veloValues = Object.values(directionTables).reduce(
      (
        acc,
        {
          data: {
            chaussee: { velo },
          },
        }
      ) => [...acc, velo],
      []
    );
    const atLeastOneVeloInputIsNotFilled = veloValues.includes("");
    if (atLeastOneVeloInputIsNotFilled) {
      setAlerts((alerts) => [
        ...alerts,
        {
          key: "veloObligatoire",
          type: "danger",
          children:
            "Veuillez renseigner tous les comptages de vélos sur chaussée",
        },
      ]);
    }

    if (allCellsAreEmpty || atLeastOneVeloInputIsNotFilled) return;

    const dateToTime = (value) => {
      const timeUnits = value.toTimeString().split(" ")[0].split(":");
      const [hh, mm, ss] = timeUnits;
      return `${hh}:${mm}:${ss}`;
    };

    const body = JSON.stringify({
      date: date.toISOString().split("T")[0],
      begin_time: dateToTime(beginTime),
      end_time: dateToTime(endTime),
      location: location,
      data: directionTables,
      metadata: {
        traffic: traffic,
        rain: rain,
        wind: wind,
        visibility: visibility,
      },
    });
    fetch(`${import.meta.env.VITE_API_ENDPOINT}/api/submit_bike_data`, {
      method: "POST",
      body,
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem("_auth"))}`,
      },
    })
      .then((resp) => resp.blob())
      .then(() => {
        setDate(new Date());
        setBeginTime(new Date());
        setEndTime(new Date());
        setLocation("");
        setTraffic("");
        setRain("");
        setWind("");
        setVisibility("");
      })
      .catch(console.error);
  };

  return (
    <>
      <h2>Adava - Formulaire de comptage</h2>

      <form
        onSubmit={handleSubmit}
        className={clsx("row", "g-3", wasValidated && "was-validated")}
        noValidate
      >
        <DateTime
          {...{ date, setDate, beginTime, setBeginTime, endTime, setEndTime }}
        />
        <Weather
          {...{
            rainOptions,
            rain,
            setRain,
            windOptions,
            wind,
            setWind,
            visibilityOptions,
            visibility,
            setVisibility,
          }}
        />
        <Trafic {...{ trafficOptions, traffic, setTraffic }} />
        <CollectedData
          {...{
            locations,
            location,
            setLocation,
            listDirectionTables,
            directionTables,
            setTableValue,
          }}
        />

        {alerts.length
          ? alerts.map(({ key, ...alert }) => <Alert key={key} {...alert} />)
          : null}

        <div className="col-12 mt-5">
          <button type="submit" className="btn btn-primary btn-lg">
            Valider
          </button>
        </div>
      </form>
    </>
  );
}

export default FormPage;
