import { Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import Navigation from "./components/layout/Navigation";
import Accueil from "./pages/Accueil";
import Formulaire from "./pages/Formulaire";
import Connexion from "./pages/Connexion";
import Graphs from "./pages/Graphs";
import { RequireAuth } from "react-auth-kit";

function App() {
  return (
    <>
      <Navigation />
      <div className="container-xl mt-5 mb-5">
        <Routes>
          <Route path="/" element={<Accueil />} />
          <Route path="connexion" element={<Connexion />} />
          <Route
            path="formulaire"
            element={
              <RequireAuth loginPath="/connexion">
                <Formulaire />
              </RequireAuth>
            }
          />
          <Route
            path="graphs"
            element={
              <RequireAuth loginPath="/connexion">
                <Graphs />
              </RequireAuth>
            }
          />
        </Routes>
      </div>
    </>
  );
}

export default App;
