export const trafficOptions = [
  {
    label: "faible",
    value: "trafic-low",
  },
  {
    label: "normal",
    value: "trafic-normal",
  },
  {
    label: "dense",
    value: "trafic-dense",
  },
  {
    label: "saturé",
    value: "trafic-saturated",
  },
];

export const rainOptions = [
  {
    label: "oui",
    value: "rain-yes",
  },
  {
    label: "non",
    value: "rain-no",
  },
];

export const windOptions = [
  {
    label: "nul",
    value: "wind-null",
  },
  {
    label: "faible",
    value: "wind-low",
  },
  {
    label: "fort",
    value: "wind-strong",
  },
];

export const visibilityOptions = [
  {
    label: "jour",
    value: "visibility-daylight",
  },
  {
    label: "pénombre",
    value: "visibility-twilight",
  },
  {
    label: "nuit",
    value: "visibility-night",
  },
];

export const modeDeTransportOptions = [
  {
    label: "Vélos",
    value: "velo",
  },
  {
    label: "VAE",
    value: "vae",
  },
  {
    label: "Trottinettes",
    value: "trottinette",
  },
  {
    label: "Autres",
    value: "autre",
  },
];

export const typeDeVoieOptions = [
  {
    label: "Chaussée",
    value: "chaussee",
  },
  {
    label: "Trottoir",
    value: "trottoir",
  },
  {
    label: "Piste cyclable",
    value: "piste_cyclable",
  },
];
